use colored::*;
use dialoguer::Input;
use lazy_regex::regex;
use std::{process, thread, time};

// Logging conventions:
//      Level:
//          Error = red
//          Info = green
//          Verbose = bright_black
//      Text:
//          Error = bright_red
//          Info = white
//          Verbose = blue
//      Data (paths, numbers, etc):
//          yellow

#[derive(Debug, PartialEq, PartialOrd, Clone, Copy)]
pub enum LogLevel {
    Verbose,
    Standard,
    Quiet,
}

impl LogLevel {
    pub fn is_verbose(&self) -> bool {
        matches!(self, LogLevel::Verbose)
    }

    pub fn is_standard(&self) -> bool {
        matches!(self, LogLevel::Standard)
    }

    pub fn is_quiet(&self) -> bool {
        matches!(self, LogLevel::Quiet)
    }
}

pub fn run(interface: String, destination: String, log_level: LogLevel) {
    // Build the command
    if log_level.is_verbose() {
        println!(
            "{} {}",
            "[Verbose]:".bright_black(),
            "Creating ping command...".blue()
        );
    }
    let ping_command = format!("ping -w 1 -c 1{} {}", interface, destination);

    // Run the ping command in loop
    if log_level.is_verbose() {
        println!(
            "{} {}{}{}{}",
            "[Verbose]:".bright_black(),
            "Pingging ".blue(),
            destination.yellow(),
            " with ".blue(),
            {
                if interface.is_empty() {
                    "*".yellow()
                } else {
                    interface[4..].yellow()
                }
            }
        );
    }
    let mut count = 0;
    loop {
        ping(&ping_command, &count, log_level, false);
        count += 1;
        thread::sleep(time::Duration::from_millis(200));
    }
}

fn ping(ping_command: &str, count: &u32, log_level: LogLevel, debug: bool) -> bool {
    // Run the ping command
    let ping_output = {
        process::Command::new("sh")
            .arg("-c")
            .arg(ping_command)
            .output()
            .expect("failed to execute ping command")
    };
    let ping_output = String::from_utf8_lossy(&ping_output.stdout);

    let re_status = regex!(r"1 received");
    let re_time = regex!(r"time=([0-9\.]+) ms");

    let status: bool = match re_status.is_match(&ping_output) {
        true => {
            if !log_level.is_quiet() && !debug {
                if log_level.is_verbose() {
                    let time = if let Some(i) = re_time.captures(&ping_output) {
                        i.get(1).unwrap().as_str()
                    } else {
                        "Unknown"
                    };

                    println!(
                        "{}: {} {} ms",
                        count.to_string().yellow(),
                        "Online".green(),
                        time.yellow()
                    );
                } else {
                    println!("{}: {}", count.to_string().yellow(), "Online".green());
                }
            }
            true
        }
        _ => {
            if !debug {
                println!("{}: {}", count.to_string().yellow(), "Offline".red());
            }
            false
        }
    };

    status
}

pub fn get_interfaces(only_list: bool, log_level: LogLevel) -> (String, Vec<String>) {
    // Print and ask which interface to use if only_list is false
    let output = {
        process::Command::new("sh")
            .arg("-c")
            .arg("ip addr")
            .output()
            .expect("failed to execute `ip addr` command")
    };
    let output = String::from_utf8_lossy(&output.stdout);
    let re = regex!(r"\d: (.*): <");
    let mut list: Vec<String> = Vec::new();
    if log_level.is_verbose() {
        println!(
            "{} {}",
            "[Verbose]:".bright_black(),
            "Found following interfaces:".blue()
        );
    }
    for (index, face) in re.captures_iter(&output).enumerate() {
        list.push(face[1].to_string());
        if log_level.is_verbose() || !only_list {
            println!("{}: {}", format!("{}", index + 1).yellow(), &face[1]);
        }
    }

    let number: usize = if !only_list {
        Input::<usize>::new()
            .with_prompt("Interface")
            .allow_empty(true)
            .interact()
            .expect("Failed to read interface number")
    } else {
        1usize
    };

    (list[number - 1].to_owned(), list)
}

pub fn debug(destination: &str, log_level: LogLevel) {
    let (_, interfaces) = get_interfaces(true, log_level);
    let mut results: Vec<bool> = Vec::new();

    if log_level.is_verbose() {
        println!("{} {}", "[Verbose]:".bright_black(), "Checking...".blue());
    }

    for interface in &interfaces {
        let ping_command = format!("ping -w 1 -c 1 -I {} {}", interface, destination);
        let count = 0;

        results.push(ping(&ping_command, &count, log_level, true));
    }

    if log_level.is_verbose() {
        println!("{} {}", "[Verbose]:".bright_black(), "Results:".blue());
    }

    for (index, interface) in interfaces.iter().enumerate() {
        let result = match results[index] {
            true => "Online".green().to_string(),
            false => "Offline".red().to_string(),
        };

        println!("{}: {}", interface.yellow(), result);
    }

    // Must exit at the end, otherwise this will just continue normally
    process::exit(0);
}
