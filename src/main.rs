use clap::{crate_version, App, Arg};
use pinnger::LogLevel;

fn main() {
    let matches = App::new("pinnger")
        .version(crate_version!())
        .author("Miika Launiainen <miika.launiainen@protonmail.com>")
        .about("Ping forever")
        .arg(
            Arg::new("verbose")
                .short('v')
                .help("Adds verbose logging")
                .required(false)
                .takes_value(false)
                .conflicts_with("quiet"),
        )
        .arg(
            Arg::new("quiet")
                .short('q')
                .help("Removes non-error logging")
                .required(false)
                .takes_value(false)
                .conflicts_with("verbose"),
        )
        .arg(
            Arg::new("debug")
                .short('D')
                .help("Debug all interfaces")
                .required(false)
                .takes_value(false)
                .conflicts_with("interface"),
        )
        .arg(
            Arg::new("interface")
                .short('i')
                .help("Select interface to test against to")
                .required(false),
        )
        .arg(
            Arg::new("destination")
                .short('d')
                .help("Destination to test againts to")
                .required(false)
                .default_value("google.com")
                .takes_value(true),
        )
        .get_matches();

    let log_level = if matches.is_present("verbose") {
        LogLevel::Verbose
    } else if matches.is_present("quiet") {
        LogLevel::Quiet
    } else {
        LogLevel::Standard
    };

    let destination: String = matches.value_of_t_or_exit("destination");

    let interface: String = if !matches.is_present("interface") {
        "".to_string()
    } else {
        let (interface, _) = pinnger::get_interfaces(false, log_level);
        format!(" -I {}", interface)
    };

    if matches.is_present("debug") {
        pinnger::debug(&destination, log_level);
    }

    pinnger::run(interface, destination, log_level);
}
